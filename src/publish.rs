use rand::Rng;
use rand::seq::SliceRandom;
use std::path::Path;
use std::ffi::OsStr;
use crate::models::HasValue;
use crate::models;
use changecase::ChangeCase;
use changecase::Case;


fn get_extension_from_filename<'ext>(filename: &'ext str, ext_list: &'ext [models::Extension]) -> &'ext str {
    match Path::new(filename).extension().and_then(OsStr::to_str) {
        Some(e) => e,
        _ => &ext_list.choose(&mut rand::thread_rng()).unwrap().value
    }
}

fn get_name_from_filename(filename: &str) -> Option<&str> {
    Path::new(filename)
        .file_stem().and_then(OsStr::to_str)
}


#[derive(Debug, Serialize, Copy, Clone)]
pub struct PublishName<'a> {
    pub input: &'a str,
    pub name: Option<&'a str>,
    pub ext: &'a str,
}

impl <'a>PublishName<'a> {
    pub fn new(input: &'a str, ext_list: &'a [models::Extension]) -> PublishName<'a> {
        PublishName {
            ext: get_extension_from_filename(&input, &ext_list),
            name: get_name_from_filename(&input),
            input: input,
        }
    }

    pub fn pick_random_suffixes<'b, T: models::HasValue>(low: u32, high: u32, separator_list: &[models::Separator], suffix_list: &[T])
                                                         -> Vec<String> where T: models::HasValue + 'b {
        let mut compound_name: Vec<String> = Vec::new();
        let mut rng = rand::thread_rng();
        let mut separator_value = String::new();
        let mut suffix_value = String::new();
        let number_of = rng.gen_range(low, high);
        for number in 0..number_of {
            let index_separator = rng.gen_range(0, separator_list.len());
            let separator = &separator_list[index_separator];
            let index_suffix = rng.gen_range(0, suffix_list.len());
            let suffix = &suffix_list[index_suffix];
            match rng.gen_range(0, 6) {
                0 => {
                    separator_value = "".to_owned();
                    suffix_value = suffix.value().to_owned().to_capitalized();
                }
                1 => {
                    separator_value = "".to_owned();
                    suffix_value = suffix.value().to_owned().to_uppercase();
                }
                _ => {
                    separator_value = separator.value().to_owned();
                    suffix_value = suffix.value().to_owned();
                }
            }
            compound_name.push(format!("{}{}", separator_value, suffix_value));
        }
        compound_name
    }

    pub fn gen_passable(&self, passable_list: Vec<models::Passable>, separator_list: Vec<models::Separator>)
                        -> String {
        let mut passable_name: Vec<String> = Vec::new();
        passable_name.push(self.name.unwrap().to_owned());
        passable_name.extend(PublishName::pick_random_suffixes(1, 3, &separator_list, &passable_list));
        passable_name.push(".".to_owned());
        passable_name.push(self.ext.to_owned());
        passable_name.join("")
    }

    pub fn gen_wow(&self, passable_list: Vec<models::Passable>, suffix_list: Vec<models::Suffix>, separator_list: Vec<models::Separator>)
                   -> String {
        let mut wow_name: Vec<String> = Vec::new();
        wow_name.extend(PublishName::pick_random_suffixes(1, 3, &separator_list, &passable_list));
        wow_name.extend(PublishName::pick_random_suffixes(1, 3, &separator_list, &suffix_list));
        let mut rng = rand::thread_rng();
        wow_name.shuffle(&mut rng);
        wow_name.insert(0, self.name.unwrap().to_owned());
        wow_name.push(".".to_owned());
        wow_name.push(self.ext.to_owned());
        wow_name.join("")
    }

    pub fn gen_hardcore(&self, passable_list: Vec<models::Passable>, suffix_list: Vec<models::Suffix>,
                        suffix2_list: Vec<models::Suffix2>, separator_list: Vec<models::Separator>)
                        -> String {
        let mut wow_name: Vec<String> = Vec::new();
        wow_name.extend(PublishName::pick_random_suffixes(1, 3, &separator_list, &passable_list));
        wow_name.extend(PublishName::pick_random_suffixes(1, 3, &separator_list, &suffix_list));
        wow_name.extend(PublishName::pick_random_suffixes(1, 3, &separator_list, &suffix2_list));
        let mut rng = rand::thread_rng();
        wow_name.shuffle(&mut rng);
        wow_name.insert(0, self.name.unwrap().to_owned());
        wow_name.push(".".to_owned());
        wow_name.push(self.ext.to_owned());
        wow_name.join("")
    }

    pub fn gen_never(&self, passable_list: Vec<models::Passable>, suffix_list: Vec<models::Suffix>,
                     suffix2_list: Vec<models::Suffix2>,
                     suffix3_list: Vec<models::Suffix3>,
                     separator_list: Vec<models::Separator>)
                     -> String {
        let mut wow_name: Vec<String> = Vec::new();
        wow_name.extend(PublishName::pick_random_suffixes(1, 3, &separator_list, &passable_list));
        wow_name.extend(PublishName::pick_random_suffixes(2, 4, &separator_list, &suffix_list));
        wow_name.extend(PublishName::pick_random_suffixes(1, 4, &separator_list, &suffix2_list));
        wow_name.extend(PublishName::pick_random_suffixes(1, 3, &separator_list, &suffix3_list));
        let mut rng = rand::thread_rng();
        wow_name.shuffle(&mut rng);
        wow_name.insert(0, self.name.unwrap().to_owned());
        wow_name.push(".".to_owned());
        wow_name.push(self.ext.to_owned());
        wow_name.join("")
    }
}

