#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate diesel;
extern crate serde_json;
extern crate rand;
extern crate changecase;

use std::path::{Path, PathBuf};
use std::collections::HashMap;
use rocket::response::NamedFile;
use rocket_contrib::{templates::Template, serve::StaticFiles};
use rocket::request::{Form, FlashMessage};
use rocket::response::{Flash, Redirect};
use rocket::Rocket;
use rocket::fairing::AdHoc;
use rocket::State;
use rocket::http::{Cookie, Cookies};
use crate::models::Passable;

mod db;
mod models;
mod publish;


// creator

#[derive(Debug, Serialize)]
struct IndexContext {
    result: Option<String>,
    input: Option<String>,
    passable: String,
    wow: String,
    hardcore: String,
    never: String,
}


#[get("/")]
fn index(conn: db::Conn) -> Template {
    let context = IndexContext {
        result: Some(" ... ".to_owned()),
        input: None,
        passable: "checked".to_owned(),
        wow: "".to_owned(),
        hardcore: "".to_owned(),
        never: "".to_owned(),
    };
    Template::render("index", &context)
}


#[post("/generator", data = "<value_form>")]
fn generator(value_form: Form<models::Gen>, conn: db::Conn) -> Template {
    let value = value_form.into_inner();
    let extensions = models::Extension::all(&conn);
    if value.input.is_empty() {
        index(conn)
    } else {
        let publish = publish::PublishName::new(&value.input, &extensions);
        let publish_name = match value.radio.as_ref() {
            "passable" => publish.gen_passable(models::Passable::all(&conn),
                                               models::Separator::all(&conn)),
            "wow" => publish.gen_wow(models::Passable::all(&conn),
                                     models::Suffix::all(&conn),
                                     models::Separator::all(&conn)),
            "hardcore" => publish.gen_hardcore(models::Passable::all(&conn),
                                               models::Suffix::all(&conn),
                                               models::Suffix2::all(&conn),
                                               models::Separator::all(&conn)),
            "never" => publish.gen_never(models::Passable::all(&conn),
                                         models::Suffix::all(&conn),
                                         models::Suffix2::all(&conn),
                                         models::Suffix3::all(&conn),
                                         models::Separator::all(&conn)),
            _ => "pas implemented".to_owned(),
        };
        let context = IndexContext {
            result: Some(publish_name),
            input: Some(publish.input.to_owned()),
            passable: match value.radio.as_ref() {
                "passable" => "checked".to_owned(),
                _ => "".to_owned()
            },
            wow: match value.radio.as_ref() {
                "wow" => "checked".to_owned(),
                _ => "".to_owned()
            },
            hardcore: match value.radio.as_ref() {
                "hardcore" => "checked".to_owned(),
                _ => "".to_owned()
            },
            never: match value.radio.as_ref() {
                "never" => "checked".to_owned(),
                _ => "".to_owned()
            },
        };
        Template::render("index", &context)
    }
}


// admin

#[derive(Debug, Serialize)]
struct Context<'a, 'b> {
    msg: Option<(&'a str, &'b str)>,
    suffixes: Vec<models::Suffix>,
    suffixes2: Vec<models::Suffix2>,
    suffixes3: Vec<models::Suffix3>,
    separators: Vec<models::Separator>,
    passables: Vec<models::Passable>,
    extensions: Vec<models::Extension>,
}

impl<'a, 'b> Context<'a, 'b> {
    pub fn err(conn: &db::Conn, msg: &'a str) -> Context<'static, 'a> {
        Context {
            msg: Some(("error", msg)),
            suffixes: models::Suffix::all(conn),
            suffixes2: models::Suffix2::all(conn),
            suffixes3: models::Suffix3::all(conn),
            separators: models::Separator::all(conn),
            passables: models::Passable::all(conn),
            extensions: models::Extension::all(conn),
        }
    }

    pub fn raw(conn: &db::Conn, msg: Option<(&'a str, &'b str)>) -> Context<'a, 'b> {
        Context {
            msg: msg,
            suffixes: models::Suffix::all(conn),
            suffixes2: models::Suffix2::all(conn),
            suffixes3: models::Suffix3::all(conn),
            separators: models::Separator::all(conn),
            passables: models::Passable::all(conn),
            extensions: models::Extension::all(conn),
        }
    }
}

#[get("/auth_page")]
fn auth_page() -> Template {
    let context: HashMap<String, String> = HashMap::new();
    Template::render("auth_page", &context)
}

#[post("/auth", data = "<auth_form>")]
fn auth(auth_form: Form<Auth>, auth: State<Auth>, mut cookies: Cookies) -> Redirect {
    let auth_struct = auth_form.into_inner();
    if auth_struct.login == auth.login && auth_struct.pwd == auth.pwd {
        cookies.add_private(Cookie::new("auth_ok", "ok"));
        Redirect::to("/admin")
    } else {
        Redirect::to("/auth_page")
    }
}

fn check_auth(mut cookies: Cookies) -> bool {
    match cookies.get_private("auth_ok") {
        Some(_) => true,
        _ => false
    }
}

#[get("/admin")]
fn admin(msg: Option<FlashMessage>, conn: db::Conn, cookies: Cookies) -> Template {
    let context = match msg {
        Some(ref msg) => Context::raw(&conn, Some((msg.name(), msg.msg()))),
        None => Context::raw(&conn, None),
    };
    match check_auth(cookies) {
        true => Template::render("admin", &context),
        false => auth_page()
    }
}

#[post("/suffixes", data = "<value_form>")]
fn new_suffix(value_form: Form<models::Value>, conn: db::Conn, cookies: Cookies) -> Flash<Redirect> {
    let value = value_form.into_inner();
    match check_auth(cookies) {
        true => {
            if value.value.is_empty() {
                Flash::error(Redirect::to("/admin"), "Value cannot be empty.")
            } else if models::Suffix::insert(value, &conn) {
                Flash::success(Redirect::to("/admin"), "")
            } else {
                Flash::error(Redirect::to("/admin"), "Whoops! The server failed.")
            }
        }
        false => Flash::error(Redirect::to("/auth_page"), "Not logged")
    }
}

#[post("/suffixes2", data = "<value_form>")]
fn new_suffix2(value_form: Form<models::Value>, conn: db::Conn, cookies: Cookies) -> Flash<Redirect> {
    let value = value_form.into_inner();
    match check_auth(cookies) {
        true => {
            if value.value.is_empty() {
                Flash::error(Redirect::to("/admin"), "Value cannot be empty.")
            } else if models::Suffix2::insert(value, &conn) {
                Flash::success(Redirect::to("/admin"), "")
            } else {
                Flash::error(Redirect::to("/admin"), "Whoops! The server failed.")
            }
        }
        false => Flash::error(Redirect::to("/auth_page"), "Not logged")
    }
}


#[post("/suffixes3", data = "<value_form>")]
fn new_suffix3(value_form: Form<models::Value>, conn: db::Conn, cookies: Cookies) -> Flash<Redirect> {
    let value = value_form.into_inner();
    match check_auth(cookies) {
        true => {
            if value.value.is_empty() {
                Flash::error(Redirect::to("/admin"), "Value cannot be empty.")
            } else if models::Suffix3::insert(value, &conn) {
                Flash::success(Redirect::to("/admin"), "")
            } else {
                Flash::error(Redirect::to("/admin"), "Whoops! The server failed.")
            }
        }
        false => Flash::error(Redirect::to("/auth_page"), "Not logged")
    }
}

#[post("/separators", data = "<value_form>")]
fn new_separator(value_form: Form<models::Value>, conn: db::Conn, cookies: Cookies) -> Flash<Redirect> {
    let value = value_form.into_inner();
    match check_auth(cookies) {
        true => {
            if value.value.is_empty() {
                Flash::error(Redirect::to("/admin"), "Value cannot be empty.")
            } else if models::Separator::insert(value, &conn) {
                Flash::success(Redirect::to("/admin"), "")
            } else {
                Flash::error(Redirect::to("/admin"), "Whoops! The server failed.")
            }
        }
        false => Flash::error(Redirect::to("/auth_page"), "Not logged")
    }
}

#[post("/passables", data = "<value_form>")]
fn new_passable(value_form: Form<models::Value>, conn: db::Conn, cookies: Cookies) -> Flash<Redirect> {
    let value = value_form.into_inner();
    match check_auth(cookies) {
        true => {
            if value.value.is_empty() {
                Flash::error(Redirect::to("/admin"), "Value cannot be empty.")
            } else if models::Passable::insert(value, &conn) {
                Flash::success(Redirect::to("/admin"), "")
            } else {
                Flash::error(Redirect::to("/admin"), "Whoops! The server failed.")
            }
        }
        false => Flash::error(Redirect::to("/auth_page"), "Not logged")
    }
}

#[post("/extensions", data = "<value_form>")]
fn new_extension(value_form: Form<models::Value>, conn: db::Conn, cookies: Cookies) -> Flash<Redirect> {
    let value = value_form.into_inner();
    match check_auth(cookies) {
        true => {
            if value.value.is_empty() {
                Flash::error(Redirect::to("/admin"), "Value cannot be empty.")
            } else if models::Extension::insert(value, &conn) {
                Flash::success(Redirect::to("/admin"), "")
            } else {
                Flash::error(Redirect::to("/admin"), "Whoops! The server failed.")
            }
        }
        false => Flash::error(Redirect::to("/auth_page"), "Not logged")
    }
}

#[delete("/suffixes/<id>")]
fn delete_suffix(id: i32, conn: db::Conn, cookies: Cookies) -> Flash<Redirect> {
    match check_auth(cookies) {
        true => {
            if models::Suffix::delete_with_id(id, &conn) {
                Flash::success(Redirect::to("/admin"), "")
            } else {
                Flash::error(Redirect::to("/admin"), "Fail to delete.")
            }
        }
        false => Flash::error(Redirect::to("/auth_page"), "Not logged")
    }
}

#[delete("/suffixes2/<id>")]
fn delete_suffix2(id: i32, conn: db::Conn, cookies: Cookies) -> Flash<Redirect> {
    match check_auth(cookies) {
        true => {
            if models::Suffix2::delete_with_id(id, &conn) {
                Flash::success(Redirect::to("/admin"), "")
            } else {
                Flash::error(Redirect::to("/admin"), "Fail to delete.")
            }
        }
        false => Flash::error(Redirect::to("/auth_page"), "Not logged")
    }
}

#[delete("/suffixes3/<id>")]
fn delete_suffix3(id: i32, conn: db::Conn, cookies: Cookies) -> Flash<Redirect> {
    match check_auth(cookies) {
        true => {
            if models::Suffix3::delete_with_id(id, &conn) {
                Flash::success(Redirect::to("/admin"), "")
            } else {
                Flash::error(Redirect::to("/admin"), "Fail to delete.")
            }
        }
        false => Flash::error(Redirect::to("/auth_page"), "Not logged")
    }
}

#[delete("/separators/<id>")]
fn delete_separator(id: i32, conn: db::Conn, cookies: Cookies) -> Flash<Redirect> {
    match check_auth(cookies) {
        true => {
            if models::Separator::delete_with_id(id, &conn) {
                Flash::success(Redirect::to("/admin"), "")
            } else {
                Flash::error(Redirect::to("/admin"), "Fail to delete.")
            }
        }
        false => Flash::error(Redirect::to("/auth_page"), "Not logged")
    }
}

#[delete("/passables/<id>")]
fn delete_passable(id: i32, conn: db::Conn, cookies: Cookies) -> Flash<Redirect> {
    match check_auth(cookies) {
        true => {
            if models::Passable::delete_with_id(id, &conn) {
                Flash::success(Redirect::to("/admin"), "")
            } else {
                Flash::error(Redirect::to("/admin"), "Fail to delete.")
            }
        }
        false => Flash::error(Redirect::to("/auth_page"), "Not logged")
    }
}

#[delete("/extensions/<id>")]
fn delete_extension(id: i32, conn: db::Conn, cookies: Cookies) -> Flash<Redirect> {
    match check_auth(cookies) {
        true => {
            if models::Extension::delete_with_id(id, &conn) {
                Flash::success(Redirect::to("/admin"), "")
            } else {
                Flash::error(Redirect::to("/admin"), "Fail to delete.")
            }
        }
        false => Flash::error(Redirect::to("/auth_page"), "Not logged")
    }
}

#[get("/<path..>", rank = 5)]
fn all(path: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/").join(path)).ok()
}

#[derive(FromForm)]
pub struct Auth {
    pub login: String,
    pub pwd: String,
}

fn rocket_launch() -> rocket::Rocket {
    let pool = db::init_pool();
    let conn = if cfg!(test) {
        Some(db::Conn(pool.get().expect("database connection for testing")))
    } else {
        println!("Fail to init db");
        None
    };
    rocket::ignite()
        .manage(pool)
        .mount("/", routes![index, admin, all, new_suffix, new_suffix2, new_suffix3, new_separator,
                                         new_passable, generator, new_extension,
                                         delete_suffix, delete_suffix2, delete_suffix3, delete_passable,
                                         delete_separator, delete_extension, auth_page, auth])
        .attach(Template::fairing())
        .attach(AdHoc::on_attach("Config Printer", |rocket| {
            let pwd = rocket.config().get_string("pwd").unwrap().to_string();
            let login = rocket.config().get_string("login").unwrap().to_string();
            Ok(rocket.manage(Auth { login, pwd }))
        }))
}

fn main() {
    rocket_launch().launch();
}
