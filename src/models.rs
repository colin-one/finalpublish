use diesel;
use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;


macro_rules! impl_value {
    (for $($t:ty),+) => {
        $(impl HasValue for $t {
            fn value(&self) -> &str {
                &self.value
            }
        })*
    }
}


mod schema {
    table! {
        suffixes (id) {
            id -> Integer,
            value -> Text,
        }
    }
    table! {
        suffixes2 (id) {
            id -> Integer,
            value -> Text,
        }
    }
    table! {
        suffixes3 (id) {
            id -> Integer,
            value -> Text,
        }
    }
    table! {
        separators (id) {
            id -> Integer,
            value -> Text,
        }
    }
    table! {
        passables (id) {
            id -> Integer,
            value -> Text,
        }
    }
    table! {
        extensions (id) {
            id -> Integer,
            value -> Text,
        }
    }
}

use self::schema::suffixes;
use self::schema::suffixes2;
use self::schema::suffixes3;
use self::schema::separators;
use self::schema::passables;
use self::schema::extensions;
use self::schema::suffixes::dsl::{suffixes as all_suffixes};
use self::schema::suffixes2::dsl::{suffixes2 as all_suffixes2};
use self::schema::suffixes3::dsl::{suffixes3 as all_suffixes3};
use self::schema::separators::dsl::{separators as all_separators};
use self::schema::passables::dsl::{passables as all_passables};
use self::schema::extensions::dsl::{extensions as all_extensions};


// Suffixes 1.
#[derive(Serialize, Queryable, Insertable, Debug, Clone)]
#[table_name="suffixes"]
pub struct NewSuffix {
    pub value: String,
}

#[derive(Queryable, Debug, Serialize)]
pub struct Suffix {
    pub id: i32,
    pub value: String,
}

impl Suffix {
    pub fn all(conn: &SqliteConnection) -> Vec<Suffix> {
        all_suffixes.order(suffixes::id.desc()).load::<Suffix>(conn).unwrap()
    }

    pub fn insert(value: Value, conn: &SqliteConnection) -> bool {
        let s = NewSuffix { value: value.value};
        diesel::insert_into(suffixes::table).values(&s).execute(conn).is_ok()
    }

    pub fn delete_with_id(id: i32, conn: &SqliteConnection) -> bool {
        diesel::delete(all_suffixes.find(id)).execute(conn).is_ok()
    }
}


// Suffixes 2.
#[derive(Serialize, Queryable, Insertable, Debug, Clone)]
#[table_name="suffixes2"]
pub struct NewSuffix2 {
    pub value: String,
}

#[derive(Queryable, Debug, Serialize)]
pub struct Suffix2 {
    pub id: i32,
    pub value: String,
}

impl Suffix2 {
    pub fn all(conn: &SqliteConnection) -> Vec<Suffix2> {
        all_suffixes2.order(suffixes2::id.desc()).load::<Suffix2>(conn).unwrap()
    }

    pub fn insert(value: Value, conn: &SqliteConnection) -> bool {
        let s = NewSuffix2 { value: value.value};
        diesel::insert_into(suffixes2::table).values(&s).execute(conn).is_ok()
    }

    pub fn delete_with_id(id: i32, conn: &SqliteConnection) -> bool {
        diesel::delete(all_suffixes2.find(id)).execute(conn).is_ok()
    }
}


// Suffixes 3.
#[derive(Serialize, Queryable, Insertable, Debug, Clone)]
#[table_name="suffixes3"]
pub struct NewSuffix3 {
    pub value: String,
}

#[derive(Queryable, Debug, Serialize)]
pub struct Suffix3 {
    pub id: i32,
    pub value: String,
}

impl Suffix3 {
    pub fn all(conn: &SqliteConnection) -> Vec<Suffix3> {
        all_suffixes3.order(suffixes3::id.desc()).load::<Suffix3>(conn).unwrap()
    }

    pub fn insert(value: Value, conn: &SqliteConnection) -> bool {
        let s = NewSuffix3 { value: value.value};
        diesel::insert_into(suffixes3::table).values(&s).execute(conn).is_ok()
    }

    pub fn delete_with_id(id: i32, conn: &SqliteConnection) -> bool {
        diesel::delete(all_suffixes3.find(id)).execute(conn).is_ok()
    }
}


// Separators
#[derive(Serialize, Queryable, Insertable, Debug, Clone)]
#[table_name="separators"]
pub struct NewSeparator {
    pub value: String,
}

#[derive(Queryable, Debug, Serialize)]
pub struct Separator {
    pub id: i32,
    pub value: String,
}

impl Separator {
    pub fn all(conn: &SqliteConnection) -> Vec<Separator> {
        all_separators.order(separators::id.desc()).load::<Separator>(conn).unwrap()
    }

    pub fn insert(value: Value, conn: &SqliteConnection) -> bool {
        let s = NewSeparator { value: value.value};
        diesel::insert_into(separators::table).values(&s).execute(conn).is_ok()
    }

    pub fn delete_with_id(id: i32, conn: &SqliteConnection) -> bool {
        diesel::delete(all_separators.find(id)).execute(conn).is_ok()
    }
}


// Passables
#[derive(Serialize, Queryable, Insertable, Debug, Clone)]
#[table_name="passables"]
pub struct NewPassable {
    pub value: String,
}

#[derive(Queryable, Debug, Serialize)]
pub struct Passable {
    pub id: i32,
    pub value: String,
}

impl Passable {
    pub fn all(conn: &SqliteConnection) -> Vec<Passable> {
        all_passables.order(passables::id.desc()).load::<Passable>(conn).unwrap()
    }

    pub fn insert(value: Value, conn: &SqliteConnection) -> bool {
        let s = NewPassable { value: value.value};
        diesel::insert_into(passables::table).values(&s).execute(conn).is_ok()
    }

    pub fn delete_with_id(id: i32, conn: &SqliteConnection) -> bool {
        diesel::delete(all_passables.find(id)).execute(conn).is_ok()
    }
}


// Extensions
#[derive(Serialize, Queryable, Insertable, Debug, Clone)]
#[table_name="extensions"]
pub struct NewExtension {
    pub value: String,
}

#[derive(Queryable, Debug, Serialize)]
pub struct Extension {
    pub id: i32,
    pub value: String,
}

impl Extension {
    pub fn all(conn: &SqliteConnection) -> Vec<Extension> {
        all_extensions.order(extensions::id.desc()).load::<Extension>(conn).unwrap()
    }

    pub fn insert(value: Value, conn: &SqliteConnection) -> bool {
        let s = NewExtension { value: value.value};
        diesel::insert_into(extensions::table).values(&s).execute(conn).is_ok()
    }

    pub fn delete_with_id(id: i32, conn: &SqliteConnection) -> bool {
        diesel::delete(all_extensions.find(id)).execute(conn).is_ok()
    }
}


pub trait HasValue {
    fn value(&self) -> &str;
}


impl_value!(for Passable, Suffix, Suffix2, Suffix3, Separator, Extension);

#[derive(FromForm)]
pub struct Value {
    pub value: String,
}

#[derive(Debug, Serialize)]
#[derive(FromForm)]
pub struct Gen {
    pub radio: String,
    pub input: String,
}



